/**
 * This file is a mock database. For a real API, there should be a data storage
 * engine in use.
 */

const profiles = [];

const getPersona = personaId => {
  const profile = profiles.find(({ id }) => id === personaId);
  return Promise.resolve(profile);
};

const updatePersona = async (id, profile) => {
  const current = await getPersona(id);
  const result = Object.assign({ id }, current || {}, profile);
  if (!current) {
    profiles.push(result);
  }
  return Promise.resolve(result);
};

module.exports = {
  getPersona,
  updatePersona,
};
