const bodyParser = require('body-parser');
const express = require('express');

const db = require('./db');
const weather = require('./weather');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const notImplimented = (req, res) => res.json({ message: 'not implimented' });

// Get a persona
app.get('/:id', async (req, res) => {
  const id = req.params.id;
  const data = await db.getPersona(id);
  const location = await weather(data.latitude, data.longitude);
  res.json(Object.assign({}, data, location));
});

// Create or update a persona
app.post('/:id', async (req, res) => {
  const id = req.params.id;
  const profile = req.body;
  const data = await db.updatePersona(id, profile);
  const location = await weather(data.latitude, data.longitude);
  res.json(Object.assign({}, data, location));
});

// Do nothing
app.get('*', notImplimented);

// Start the server
app.listen(8080, () => console.log('http://localhost:8080'));
