const https = require('https');

/**
 * Requests data from api.weather.gov
 * @param {string} path URL path to append to the API
 */
function request(path) {
  return new Promise((resolve, reject) => {
    const options = {
      headers: {
        'User-Agent': 'GML-Developer-Evaluation',
        'Cache-Control': 'no-cache',
      },
      host: 'api.weather.gov',
      method: 'GET',
      path,
      protocol: 'https:',
    };

    const req = https.request(options, res => {
      let responseBody = '';

      res.setEncoding('utf-8');

      // Concat all the data into a single string.
      res.on('data', chunk => {
        responseBody += chunk;
      });

      // Return the result of the request.
      res.on('end', () => {
        try {
          resolve(JSON.parse(responseBody));
        } catch (err) {
          resolve(responseBody);
        }
      });
    });

    // Return any errors.
    req.on('error', err => reject(err));

    // Send the request.
    req.end();
  });
}

/**
 * Returns city, state, and temparature for a specific location.
 * @param {number} lat Latitude
 * @param {number} lon Longitude
 */
async function getLocation(lat, lon) {
  // Get the general location meta data
  const data = await request(`/points/${lat},${lon}`);
  const {
    observationStations,
    relativeLocation: {
      properties: { city, state },
    },
  } = data.properties;

  // Get the observation stations
  const stations = await request(observationStations);

  // Use the first station
  const station = stations.features[0].id;

  // Get the latest observation from the station
  const latest = await request(`${station}/observations/latest`);
  const celsius = latest.properties.temperature.value;

  // Convert to fahrenheit
  const fahrenheit = (celsius * 9) / 5 + 32;

  return {
    city,
    state,
    temperature: {
      fahrenheit,
      celsius,
    },
  };
}

module.exports = getLocation;
