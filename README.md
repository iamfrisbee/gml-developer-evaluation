# Grey Market Labs Developer Evaluation

Developed from 21:50 until 23:16.

## Table of Contents

1. [API](#api)
1. [Questions and Answers](#questions-and-answers)

## API

All requests return a profile object like this example.

```javascript
{
    "id": "abc31234",
    "firstName": "Mickey",
    "lastname": "Mouse",
    "interests": [
        "cartoons",
        "theme parks",
        "pluto"
    ],
    "latitude": 28.3852,
    "longitude": -81.5639,
    "city": "Bay Lake",
    "state": "FL",
    "temperature": {
        "fahrenheit": 82.04000000000002,
        "celsius": 27.80000000000001
    }
}
```

To retrieve a profile, use a `GET` request in following format and replace `abcd1234` with the unique persona ID.

`http://localhost:8080/abcd1234`

To update or create a new profile, use a `POST` request to the same URL with a JSON body in the format of the profile above. The ID will be overwritten by the one in the URL. Also, only the fields provided will be updated. Any field not provided will maintain its previous value, if one existed.

## Questions and Answers

**Where you able to complete all the functionality in the time alloted? If not, which pieces are outstanding?**

Though I did complete the required functionality, there is plenty more I would do before considering this completed.

**What potential issues do you see with your API, as implemented? How would you address them?**

1. Database - There is a file named `db.js` that acts as a mock database. Everything is stored in a local variable. Data should be retreived from and inserted into a database for persistent storage. Mongo DB being the easiest to implement would be a good suggestion, though security with Mongo DB is very expensive. Seeing as this is fake information, security is not likely that important. If security is more important than I believe, then I would suggest Microsoft SQL Server instead. The driver is not as stable, but the storage is less costly to protect and data lost during transactions is much less likely.
2. Security - There isn't any. Again, being fake data, this may not be important. That said, generally speaking, there should be some sort of authentication. A JWT or OAUTH being the most common, but even a simple API key of some sort is better than nothing.
3. Error handling - There isn't any. Errors should be logged to some sort of logging server (splunk is nice) or at the very least a text file in order to troubleshoot and debug issues if they should arise. Also, user friendly error messages should be returned from the API. For example, a request for a profile that doesn't exist should return a message letting the user know he has used an invalid ID.
4. Unit testing - I did not complete any unit testing. This is an important piece to have. It is best to have very thorough unit testing to avoid bugs with production releases due to overlooking areas that may be affected by a code change.

**What security limitations does your API have?**

There is no authentication at all. Anyone and everyone could pull the information assuming they had the proper persona ID. That is the easiest answer. There are many environment questions that would need to be answered for me to go any further.

For example:

In a scenario where all traffic goes directly to the server and the firewall only acts as a router, DDoS attacks could be a major issue. Node JS is single threaded so enough traffic could bring it down pretty easily. Hopefully, the hardware in front of the server is checking for DDoS in some way and mitigating those attacks.

Also, what other ports are open on the server? If the SSH port or FTP is open, how good are the passwords? Does root have access to FTP or SSH?

In the grand scheme of things, the information stored here is not valuable. It is useful to a small group of people, but it would provide no benefit to anyone else to acquire so though there is no security, does there really need to be security?
